﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05._12task8
{
    class Program
    {
        static void Main(string[] args)
        {
            bool prost = true;
            int i = 2;
            Console.Write("Введите число: ");
            int n = int.Parse(Console.ReadLine());
            while (i<=(Math.Sqrt(n)))
            {
                if (n % i == 0)
                {
                    prost = false;
                    break;
                }
                i++;
            }
            if (prost)
            {
                Console.WriteLine("Число простое");
            }
            else
            {
                Console.WriteLine("Число составное");
            }
            
            Console.ReadKey();
        }
    }
}
